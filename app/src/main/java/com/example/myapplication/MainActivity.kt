package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var gameScoreView: TextView
    private lateinit var tapButton: Button
    private lateinit var startGameButton: Button

    private lateinit var countDownTimer: CountDownTimer
    private var counterInterval: Long = 1000
    private var actualTime = 0
    private var random_number = 0
    private var gameScore = 0
    private var isGameStarted = false
    private var timeLeft = 10
    private val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG, "on create called, score: $gameScore")

        gameScoreView = findViewById(R.id.score)
        startGameButton = findViewById(R.id.start_game_button)
        tapButton = findViewById(R.id.tap_button)
        tapButton.isEnabled = false

        startGameButton.setOnClickListener{
            startGame()
            tapButton.isEnabled = true
        }

        tapButton.setOnClickListener{
            incrementScore()
        }

        if(savedInstanceState != null){
            gameScore = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            isGameStarted = savedInstanceState.getBoolean(GAME_STARTED)
            restoreGame()
        }else{
            resetGame()
        }

    }

    private fun startGame(){
        countDownTimer.start()
        isGameStarted = true
        random_number = (0..10).shuffled().first()
    }

    private fun incrementScore(){
        if(random_number == actualTime){
            gameScore = gameScore+100
        }
        if (Math.abs(random_number - actualTime) == 1 ){
            gameScore = gameScore+50
        }
    }

    private fun configCountDownTimer(){
        countDownTimer = object : CountDownTimer((timeLeft*1000).toLong(),counterInterval){
            override fun onTick(millisUntilFinished: Long) {
                actualTime = 10 - (millisUntilFinished.toInt() / 1000)
            }

            override fun onFinish() {
                endGame()
            }
        }
    }

    private fun resetGame(){
        gameScore = 0
        gameScoreView.text = getString(R.string.score, gameScore)

        timeLeft = 10

        configCountDownTimer()

        isGameStarted = false
    }

    private fun endGame(){
        resetGame()
    }


    private fun restoreGame(){
        gameScoreView.text = getString(R.string.score, gameScore)

        configCountDownTimer()

        if(isGameStarted){
            countDownTimer.start()
        }
    }

    companion object{
        private const val SCORE_KEY = "SCORE_KEY"
        private const val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        private const val GAME_STARTED = "GAME_STARTED"
    }

}
